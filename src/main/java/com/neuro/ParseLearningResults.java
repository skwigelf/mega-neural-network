package com.neuro;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseLearningResults {
    public static void main(String[] args) {
        try {
            FileReader fileReader = new FileReader("results.txt");
            BufferedReader reader = new BufferedReader(fileReader);

            List<BigDecimal> goodResults = new ArrayList<>();
            List<BigDecimal> badResults = new ArrayList<>();
            String line = reader.readLine();
            Pattern pattern = Pattern.compile("Actual result for image : ([0-9,.]*), expected : ([0-9,.]*)");
            while (line != null) {
                System.out.println(line);
                line = reader.readLine();
                Matcher matcher = pattern.matcher(line);
                BigDecimal actual = new BigDecimal(matcher.group(0));
                BigDecimal expected = new BigDecimal(matcher.group(1));
                BigDecimal delta = actual.subtract(expected);
                if (delta.abs().compareTo(BigDecimal.valueOf(0.03)) <= 0) {
                    goodResults.add(delta);
                } else {
                    badResults.add(delta);
                }
            }

            System.out.println("Good results : " + goodResults.size());
            System.out.println("Bad results : " + badResults.size());
            System.out.println("Percents of good : " + (double) goodResults.size() / (double) (badResults.size() + goodResults.size()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
