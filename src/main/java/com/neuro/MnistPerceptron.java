package com.neuro;

import com.neuro.ioutils.MatrixParser;
import com.neuro.ioutils.ParsedMatrix;
import com.neuro.math.Matrix;
import com.neuro.neural.ActivationFunctions;
import com.neuro.neural.NeuralNetworkLearning;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleFunction;
import java.util.regex.Pattern;

import static com.neuro.math.MathUtilsDouble.*;

public class MnistPerceptron {
    public static void main(String[] args) {
        try {
            MatrixParser matrixParser = new MatrixParser();
            List<ParsedMatrix<Double>> matrixList = matrixParser.readData("data/train-images.idx3-ubyte",
                    "data/train-labels.idx1-ubyte");

            List<ParsedMatrix<Double>> expectMatrixList = matrixParser.readData("data/t10k-images.idx3-ubyte",
                    "data/t10k-labels.idx1-ubyte");

            BufferedWriter writer = new BufferedWriter(new FileWriter("results.txt"));
            double learningRate = 0.02d;
            Matrix<Double> firstLayerWeightsMatrix = initLayerWeights(784, 128);
            Matrix<Double> secondLayerWeightsMatrix = initLayerWeights(128, 1);

            writer.write("Learning started");
            for (int ep = 0; ep < 1000; ep++) {
                for (int i = 0; i < matrixList.size(); i++) {
                    Matrix<Double> vectorInput = matrixList.get(i).matrixToList();
                    Matrix<Double> firstLayerResultMatrix = multiplyMatrix(vectorInput, firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
                    Matrix<Double> secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);

                    double delta = secondLayerResultMatrix.getElement(0, 0) - ((double) matrixList.get(i).getLabel());
                    Matrix<Double> errorLayer_2 = createMatrixOneDouble(delta);
                    Matrix<Double> weightDeltaLayer_2 = scalarMultiplyMatrix(errorLayer_2,
                            evalFunctionForAllMatrixValues(secondLayerResultMatrix, ActivationFunctions::sigmoidDx));
                    //Matrix<Double> errorDeltaMatrixSecond = NeuralNetworkLearning.calculateErrorsForLayer(secondLayerResultMatrix, errorLayer_2);
                    secondLayerWeightsMatrix = NeuralNetworkLearning.trainFullyConnectedLayer(secondLayerWeightsMatrix,
                            weightDeltaLayer_2, firstLayerResultMatrix.translate(), learningRate);

                    Matrix<Double> errorLayer_1 = multiplyEveryElement(weightDeltaLayer_2, secondLayerWeightsMatrix.translate());
                    Matrix<Double> weightDeltaLayer_1 = scalarMultiplyMatrix(errorLayer_1,
                            evalFunctionForAllMatrixValues(firstLayerResultMatrix, ActivationFunctions::sigmoidDx));
                    firstLayerWeightsMatrix = NeuralNetworkLearning.trainFullyConnectedLayer(firstLayerWeightsMatrix,
                            weightDeltaLayer_1, vectorInput.translate(), learningRate);
                }
            }
            writer.write("Learning finished");


            writer.close();
            for (ParsedMatrix<Double> matrix : expectMatrixList) {
                Matrix<Double> vectorInput = matrix.matrixToList();
                Matrix<Double> firstLayerResultMatrix = multiplyMatrix(vectorInput, firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
                Matrix<Double> secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);
                writer.write("Actual result for image : " + String.valueOf(secondLayerResultMatrix.getElement(0, 0))
                        + ", expected : " + String.valueOf(matrix.getLabel()) + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Matrix<Double> initLayerWeights(int inputSize, int neuronInHiddenLayerNum) {
        Matrix<Double> result = new Matrix<>();
        for (int rowNum = 0; rowNum < inputSize; rowNum++) {
            List<Double> row = new ArrayList<>();
            for (int colNum = 0; colNum < neuronInHiddenLayerNum; colNum++) {
                row.add(Math.random());
            }
            result.addRow(row);
        }
        return result;
    }

    private static Matrix<Double> createMatrixOneDouble(double value) {
        Matrix<Double> matrix = new Matrix<>();
        List<Double> list = new ArrayList<>();
        list.add(value);
        matrix.addRow(list);
        return matrix;
    }

    @SuppressWarnings("Duplicates")
    private static Matrix<Double> evalFunctionForAllMatrixValues(Matrix<Double> matrix, DoubleFunction<Double> function) {
        Matrix<Double> newMatrix = new Matrix<>();
        for (int y = 0; y < matrix.getSizeRow(); y++) {
            List<Double> row = new ArrayList<>();
            for (int x = 0; x < matrix.getSizeCol(); x++) {
                row.add(function.apply(matrix.getElement(x, y)));
            }
            newMatrix.addRow(row);
        }
        return newMatrix;
    }
}
