package com.neuro.neural;

import com.neuro.math.Matrix;

import java.util.List;
import java.util.stream.IntStream;

import static com.neuro.math.MathUtilsInteger.*;
import static java.util.stream.Collectors.toList;

public class NeuralNetwork {
    public static List<Matrix<Integer>> convolutionalLayerInteger(
            Matrix<Integer> inputMatrix,
            List<Matrix<Integer>> kernels) {
        return kernels.stream().map(kernel -> multiplyConvolutionalLayer(inputMatrix, kernel)).collect(toList());
    }

    public static List<Matrix<Integer>> convolutionalLayerInteger(
            List<Matrix<Integer>> matrices,
            List<Matrix<Integer>> kernels) {
        return IntStream.rangeClosed(0, matrices.size())
                .mapToObj(index -> multiplyConvolutionalLayer(matrices.get(index), kernels.get(index))).collect(toList());
    }

    public static List<Matrix<Integer>> maxPoolLayer(List<Matrix<Integer>> layerMatrixes, int size, int stride) {
        return layerMatrixes.stream().map(matrix -> maxPool(matrix, size, stride)).collect(toList());
    }
}
