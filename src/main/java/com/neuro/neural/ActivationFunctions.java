package com.neuro.neural;

import static java.lang.Math.pow;

public class ActivationFunctions {
    private static double EXPONENT = 2.7182818;

    public static Double reLu(double inputValue) {
        return inputValue < 0 ? 0 : inputValue;
    }

    public static Double sigmoid(double inputValue) {
        return 1 / (1 + pow(EXPONENT, -inputValue));
    }

    public static Double sigmoidDx(Double inputValue) {
        return sigmoid(inputValue) * (1 - sigmoid(inputValue));
    }
}
