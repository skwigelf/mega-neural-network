package com.neuro.neural;

import com.neuro.math.MathUtilsDouble;
import com.neuro.math.Matrix;

public class NeuralNetworkLearning {
    public static Matrix<Double> calculateErrorsForLayer(Matrix<Double> layer, Matrix<Double> errorMatrix) {
        return MathUtilsDouble.multiplyMatrix(errorMatrix, layer);
    }

    public static Matrix<Double> trainFullyConnectedLayer(Matrix<Double> weights, Matrix<Double> weightDeltaLayer, Matrix<Double> values
            , double learningRate) {
        Matrix<Double> newWeights = MathUtilsDouble.multiplyMatrix(values, weightDeltaLayer);
        return MathUtilsDouble.scalarMinusMatrix(weights, newWeights);
        /*for (int y = 0; y < weights.getSizeY(); y++) {
            List<Double> row = new ArrayList<>(weights.getSizeY());
            for (int x = 0; x < weights.getSizeX(); x++) {
                double newWeight = weights.getElement(x, y) - (learningRate * values.getElement(y, 0) *
                        weightDeltaLayer.getElement(x, 0));
                row.add(newWeight);
            }
            newWeights.addRow(row);
        }*/

        //return newWeights;
    }
}
