package com.neuro.math;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleFunction;

public class MathUtilsDouble {
    public static Matrix<Double> multiplyMatrix(Matrix<Double> firstMatrix, Matrix<Double> secondMatrix
            , DoubleFunction<Double> activationFunction) {
        Matrix<Double> resultMatrix = new Matrix<>();
        if (firstMatrix.getSizeCol() != secondMatrix.getSizeRow()) {
            throw new MatrixException("Invalid size of matrix to multiply");
        }
        for (int firstMatrixRow = 0; firstMatrixRow < firstMatrix.getSizeRow(); firstMatrixRow++) {
            List<Double> rowList = new ArrayList<>();
            for (int secondMatrixCol = 0; secondMatrixCol < secondMatrix.getSizeCol(); secondMatrixCol++) {
                double sum = 0;
                for (int index = 0; index < secondMatrix.getSizeRow(); index++) {
                    sum += firstMatrix.getElement(index, firstMatrixRow) * secondMatrix.getElement(secondMatrixCol, index);
                }
                rowList.add(activationFunction.apply(sum));
            }
            resultMatrix.addRow(rowList);
        }
        return resultMatrix;
    }

    public static Matrix<Double> multiplyMatrix(Matrix<Double> firstMatrix, Matrix<Double> secondMatrix) {
        return multiplyMatrix(firstMatrix, secondMatrix, x -> x);
    }

    public static Matrix<Double> scalarMultiplyMatrix(Matrix<Double> firstMatrix, Matrix<Double> secondMatrix) {
        if (firstMatrix.getSizeCol() != secondMatrix.getSizeCol() && firstMatrix.getSizeRow() != secondMatrix.getSizeRow()) {
            throw new MatrixException("Matrices sizes must be equal");
        }
        Matrix<Double> resultMatrix = new Matrix<>(firstMatrix.getSizeRow());
        for (int y = 0; y < firstMatrix.getSizeRow(); y++) {
            List<Double> row = new ArrayList<>();
            for (int x = 0; x < secondMatrix.getSizeCol(); x++) {
                row.add(firstMatrix.getElement(x, y) * secondMatrix.getElement(x, y));
            }
            resultMatrix.addRow(row);
        }
        return resultMatrix;
    }

    public static Matrix<Double> scalarPlusMatrix(Matrix<Double> firstMatrix, Matrix<Double> secondMatrix) {
        if (firstMatrix.getSizeCol() != secondMatrix.getSizeCol() && firstMatrix.getSizeRow() != secondMatrix.getSizeRow()) {
            throw new MatrixException("Matrices sizes must be equal");
        }
        Matrix<Double> resultMatrix = new Matrix<>(firstMatrix.getSizeRow());
        for (int y = 0; y < firstMatrix.getSizeRow(); y++) {
            List<Double> row = new ArrayList<>();
            for (int x = 0; x < secondMatrix.getSizeCol(); x++) {
                row.add(firstMatrix.getElement(x, y) + secondMatrix.getElement(x, y));
            }
            resultMatrix.addRow(row);
        }
        return resultMatrix;
    }

    public static Matrix<Double> scalarMinusMatrix(Matrix<Double> firstMatrix, Matrix<Double> secondMatrix) {
        if (firstMatrix.getSizeCol() != secondMatrix.getSizeCol() && firstMatrix.getSizeRow() != secondMatrix.getSizeRow()) {
            throw new MatrixException("Matrices sizes must be equal");
        }
        Matrix<Double> resultMatrix = new Matrix<>(firstMatrix.getSizeRow());
        for (int y = 0; y < firstMatrix.getSizeRow(); y++) {
            List<Double> row = new ArrayList<>();
            for (int x = 0; x < secondMatrix.getSizeCol(); x++) {
                row.add(firstMatrix.getElement(x, y) - secondMatrix.getElement(x, y));
            }
            resultMatrix.addRow(row);
        }
        return resultMatrix;
    }

    public static Matrix<Double> multiplyConvolutionalLayer(Matrix<Double> layer, Matrix<Double> kernel) {
        /*if (sideSize == kernel.getSizeX()) {
            throw new MatrixException("Invalid size");
        }*/
        int sideSize = kernel.getSizeCol();
        Matrix<Double> resultMatrix = new Matrix<>();
        for (int i = 0; i < layer.getSizeCol() + 1 - sideSize; i++) {
            List<Double> row = new ArrayList<>();
            for (int j = 0; j < layer.getSizeRow() + 1 - sideSize; j++) {
                Matrix<Double> subMatrix = layer.extractSubMatrixFromMatrix(i, j, i + sideSize, j + sideSize);
                row.add(evaluateConvolutional(subMatrix, kernel));
            }
            resultMatrix.addRow(row);
        }
        return resultMatrix;
    }

    public static Double evaluateConvolutional(Matrix<Double> layer, Matrix<Double> conv) {
        double sum = 0;
        for (int i = 0; i < layer.getSizeCol(); i++) {
            for (int j = 0; j < layer.getSizeRow(); j++) {
                sum += layer.getElement(i, j) * conv.getElement(i, j);
            }
        }
        return sum;
    }

    public static Matrix<Double> maxPool(Matrix<Double> matrix, int size, int stride) {
        if (matrix.getSizeCol() != matrix.getSizeRow()) {
            throw new IllegalStateException("Matrix sides should be equal");
        }
        /*IntStream.range(0, matrix.getSizeX() - size)
                .mapToObj(xSide ->
                        IntStream.range(0, matrix.getSizeY() -size)
                                .mapToObj(ySide ->
                                        IntStream.rangeClosed(0, stride)).)*/
        Matrix<Double> resultMatrix = new Matrix<>();
        for (int xSide = 0; xSide < matrix.getSizeCol() + 1 - size; xSide += stride) {
            List<Double> row = new ArrayList<>();
            for (int ySide = 0; ySide < matrix.getSizeRow() + 1 - size; ySide += stride) {
                row.add(getMaxElement(matrix.extractSubMatrixFromMatrix(xSide, ySide, xSide + size, ySide + size)));
            }
            resultMatrix.addRow(row);
        }
        return resultMatrix;
    }

    public static Double getMaxElement(Matrix<Double> matrix) {
        //return matrix.getMatrix().stream().flatMap(Collection::stream).reduce(0, Double::sum);
        Double maxElement = matrix.getElement(0, 0);
        for (List<Double> row : matrix.getMatrix()) {
            for (Double element : row) {
                maxElement = maxElement > element ? maxElement : element;
            }
        }
        return maxElement;
    }

    public static Matrix<Double> multiplyEveryElement(Matrix<Double> firstMatrix, Matrix<Double> secondMatrix) {
        Matrix<Double> result = new Matrix<>();
        for (int y = 0; y < secondMatrix.getSizeRow(); y++) {
            List<Double> row = new ArrayList<>();
            for (int x = 0; x < secondMatrix.getSizeCol(); x++) {
                row.add(secondMatrix.getElement(x, y) * firstMatrix.getElement(0, 0));
            }
            result.addRow(row);
        }
        return result;
    }


    public static int[] xnor(int[] row, int[] convolution) {
        int[] result = new int[3];
        for (int i = 0; i < row.length; i++) {
            result[i] = row[i] == convolution[i] ? 1 : 0;
        }
        return result;
    }

    public static int popCount(int[] bits) {
        int ones = 0;
        int zeros = 0;
        for (int i = 0; i < bits.length; i++) {
            if (bits[i] == 1) {
                ones++;
            } else {
                zeros++;
            }
        }
        return ones - zeros;
    }

}
