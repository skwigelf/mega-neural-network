package com.neuro.math;

import java.util.ArrayList;
import java.util.List;

public class MathUtilsInteger {
    public static Matrix<Integer> multiplyMatrix(Matrix<Integer> firstMatrix, Matrix<Integer> secondMatrix) {
        Matrix<Integer> resultMatrix = new Matrix<>();
        if (firstMatrix.getSizeCol() != secondMatrix.getSizeRow()) {
            throw new MatrixException("Invalid size of matrix to multiply");
        }
        for (int firstMatrixRow = 0; firstMatrixRow < firstMatrix.getSizeRow(); firstMatrixRow++) {
            List<Integer> rowList = new ArrayList<>();
            for (int secondMatrixCol = 0; secondMatrixCol < secondMatrix.getSizeCol(); secondMatrixCol++) {
                int sum = 0;
                for (int index = 0; index < secondMatrix.getSizeRow(); index++) {
                    sum += firstMatrix.getElement(index, firstMatrixRow) * secondMatrix.getElement(secondMatrixCol, index);
                }
                rowList.add(sum);
            }
            resultMatrix.addRow(rowList);
        }
        return resultMatrix;
    }

    public static Matrix<Integer> multiplyConvolutionalLayer(Matrix<Integer> layer, Matrix<Integer> kernel) {
        /*if (sideSize == kernel.getSizeX()) {
            throw new MatrixException("Invalid size");
        }*/
        int sideSize = kernel.getSizeCol();
        Matrix<Integer> resultMatrix = new Matrix<>();
        for (int i = 0; i < layer.getSizeCol() + 1 - sideSize; i++) {
            List<Integer> row = new ArrayList<>();
            for (int j = 0; j < layer.getSizeRow() + 1 - sideSize; j++) {
                Matrix<Integer> subMatrix = layer.extractSubMatrixFromMatrix(i, j, i + sideSize, j + sideSize);
                row.add(evaluateConvolutional(subMatrix, kernel));
            }
            resultMatrix.addRow(row);
        }
        return resultMatrix;
    }

    public static Integer evaluateConvolutional(Matrix<Integer> layer, Matrix<Integer> conv) {
        int sum = 0;
        for (int i = 0; i < layer.getSizeCol(); i++) {
            for (int j = 0; j < layer.getSizeRow(); j++) {
                sum += layer.getElement(i, j) * conv.getElement(i, j);
            }
        }
        return sum;
    }

    public static Matrix<Integer> maxPool(Matrix<Integer> matrix, int size, int stride) {
        if (matrix.getSizeCol() != matrix.getSizeRow()) {
            throw new IllegalStateException("Matrix sides should be equal");
        }
        /*IntStream.range(0, matrix.getSizeX() - size)
                .mapToObj(xSide ->
                        IntStream.range(0, matrix.getSizeY() -size)
                                .mapToObj(ySide ->
                                        IntStream.rangeClosed(0, stride)).)*/
        Matrix<Integer> resultMatrix = new Matrix<>();
        for (int xSide = 0; xSide < matrix.getSizeCol() + 1 - size; xSide += stride) {
            List<Integer> row = new ArrayList<>();
            for (int ySide = 0; ySide < matrix.getSizeRow() + 1 - size; ySide += stride) {
                row.add(getMaxElement(matrix.extractSubMatrixFromMatrix(xSide, ySide, xSide + size, ySide + size)));
            }
            resultMatrix.addRow(row);
        }
        return resultMatrix;
    }

    public static Integer getMaxElement(Matrix<Integer> matrix) {
        //return matrix.getMatrix().stream().flatMap(Collection::stream).reduce(0, Integer::sum);
        Integer maxElement = matrix.getElement(0, 0);
        for (List<Integer> row : matrix.getMatrix()) {
            for (Integer element : row) {
                maxElement = maxElement > element ? maxElement : element;
            }
        }
        return maxElement;
    }

    public static int[] xnor(int[] row, int[] convolution) {
        int[] result = new int[3];
        for (int i = 0; i < row.length; i++) {
            result[i] = row[i] == convolution[i] ? 1 : 0;
        }
        return result;
    }

    public static int popCount(int[] bits) {
        int ones = 0;
        int zeros = 0;
        for (int i = 0; i < bits.length; i++) {
            if (bits[i] == 1) {
                ones++;
            } else {
                zeros++;
            }
        }
        return ones - zeros;
    }


}
