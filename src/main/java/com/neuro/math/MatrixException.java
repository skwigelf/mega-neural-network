package com.neuro.math;

public class MatrixException extends RuntimeException {
    public MatrixException() {
    }

    public MatrixException(String message) {
        super(message);
    }
}
