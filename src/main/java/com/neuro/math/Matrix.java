package com.neuro.math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toList;

public class Matrix<T extends Number> {
    private List<List<T>> matrix = new ArrayList<>();

    public Matrix(T[][] matrix) {
        this.matrix = Arrays.stream(matrix)
                .map(row -> Arrays.stream(row).collect(toList()))
                .collect(toList());
    }

    public Matrix() {

    }

    public Matrix(int size) {
        matrix = new ArrayList<>(size);
        matrix.forEach(x -> new ArrayList<T>(size));
    }

    public Matrix(int sizeX, int sizeY) {
        matrix = new ArrayList<>(sizeX);
        matrix.forEach(x -> new ArrayList<T>(sizeY));
    }

    public Matrix(List<List<T>> matrix) {
        this.matrix = matrix;
    }

    public int getSizeRow() {
        return matrix.size();
    }

    public int getSizeCol() {
        return matrix.isEmpty() ? 0 : matrix.get(0).size();
    }

    public List<List<T>> getMatrix() {
        return matrix;
    }

    public T getElement(int xIndex, int yIndex) {
        return matrix.get(yIndex).get(xIndex);
    }

    public Matrix<T> extractSubMatrixFromMatrix(int rowStart, int colStart, int rowEnd, int colEnd) {
        List<List<T>> subMatrix = matrix.subList(rowStart, rowEnd);
        return new Matrix<T>(subMatrix.stream().map(element -> element.subList(colStart, colEnd)).collect(toList()));
    }

    public void addRow(List<T> row) {
        matrix.add(row);
    }

    public Matrix<T> matrixToList() {
        Matrix<T> resultMatrix = new Matrix<>();
        List<T> row = matrix.stream().flatMap(Collection::stream).collect(toList());
        resultMatrix.addRow(row);
        return resultMatrix;
    }

    public void print() {
        matrix.forEach(row -> {
            StringBuilder rowString = new StringBuilder();
            for (int i = 0; i < row.size(); i++) {
                rowString.append(String.valueOf(row.get(i)));
                if (i != row.size() - 1) {
                    rowString.append(" | ");
                }
            }
            System.out.println(rowString);
        });
    }

    public Matrix<T> translate() {
        Matrix<T> result = new Matrix<>();
        for (int x = 0; x < this.getSizeCol(); x++) {
            List<T> row = new ArrayList<>();
            for (int y = 0; y < this.getSizeRow(); y++) {
                row.add(this.getElement(x, y));
            }
            result.addRow(row);
        }
        return result;
    }
    //public Matrix<T> reshape(int )

    public void forEach(Consumer<? super List<T>> action) {
        matrix.forEach(action);
    }

}
