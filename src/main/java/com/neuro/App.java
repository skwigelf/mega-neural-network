package com.neuro;

import com.neuro.math.Matrix;

import java.util.ArrayList;
import java.util.List;

import static com.neuro.neural.NeuralNetwork.convolutionalLayerInteger;
import static com.neuro.neural.NeuralNetwork.maxPoolLayer;

public class App {
    public static void main(String[] args) {
        Integer[][] input = {
                {1, 5, 4, 7, 2, 5, 8, 9, 1},
                {4, 2, 4, 7, 2, 5, 8, 9, 1},
                {7, 5, 4, 7, 2, 5, 8, 9, 1},
                {2, 5, 4, 7, 2, 5, 8, 9, 1},
                {3, 4, 7, 7, 2, 5, 8, 9, 1},
                {8, 5, 4, 7, 2, 5, 8, 9, 1},
                {3, 5, 2, 7, 2, 5, 8, 9, 1},
                {2, 5, 4, 7, 2, 5, 8, 9, 1},
                {7, 5, 4, 7, 2, 5, 8, 9, 1}};
        Matrix<Integer> inputMatrix = new Matrix<>(input);
        List<Matrix<Integer>> kernelsFirstLayer = createIntKernels();
        List<Matrix<Integer>> firstLayerResult = convolutionalLayerInteger(inputMatrix, kernelsFirstLayer);
        List<Matrix<Integer>> maxPoolFirstLayerResults = maxPoolLayer(firstLayerResult, 2, 2);
        maxPoolFirstLayerResults.forEach(Matrix::print);

        List<Matrix<Integer>> kernelsSecondLayer = createIntKernels();
        List<Matrix<Integer>> secondLayerResult = convolutionalLayerInteger(maxPoolFirstLayerResults, kernelsSecondLayer);
        List<Matrix<Integer>> maxPoolSecondLayerResult = maxPoolLayer(secondLayerResult, 2, 2);
    }

    private static List<Matrix<Integer>> createIntKernels() {
        List<Matrix<Integer>> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Integer[][] integers = {
                    {i, i, i},
                    {i, i, i},
                    {i, i, i}};
            list.add(new Matrix<>(integers));
        }
        return list;
    }
}
