package com.neuro;

import com.neuro.ioutils.ParsedMatrix;
import com.neuro.math.Matrix;
import com.neuro.neural.ActivationFunctions;
import com.neuro.neural.NeuralNetworkLearning;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleFunction;

import static com.neuro.math.MathUtilsDouble.*;

public class Perceptron {
    public static void main(String[] args) {
        double learningRate = 0.02d;
        List<ParsedMatrix<Double>> trainDataset = createTrainDataset();
        Double[][] firstLayerWeights = {
                {0.19d, 0.85d},
                {0.44d, 0.043d},
                {0.43d, 0.029d}};
        Double[][] secondLayerWeight = {
                {0.9d},
                {0.052d}};

        Matrix<Double> firstLayerWeightsMatrix = new Matrix<>(firstLayerWeights);
        Matrix<Double> secondLayerWeightsMatrix = new Matrix<>(secondLayerWeight);

        for (int epochs = 0; epochs < 2000; epochs++) {
            for (ParsedMatrix<Double> input : trainDataset) {
                Matrix<Double> firstLayerResultMatrix = multiplyMatrix(input, firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
                Matrix<Double> secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);

                double delta = secondLayerResultMatrix.getElement(0, 0) - ((double) input.getLabel());
                Matrix<Double> errorLayer_2 = createMatrixOneDouble(delta);
                Matrix<Double> weightDeltaLayer_2 = scalarMultiplyMatrix(errorLayer_2,
                        evalFunctionForAllMatrixValues(secondLayerResultMatrix, ActivationFunctions::sigmoidDx));
                //Matrix<Double> errorDeltaMatrixSecond = NeuralNetworkLearning.calculateErrorsForLayer(secondLayerResultMatrix, errorLayer_2);
                secondLayerWeightsMatrix = NeuralNetworkLearning.trainFullyConnectedLayer(secondLayerWeightsMatrix,
                        weightDeltaLayer_2, firstLayerResultMatrix.translate(), learningRate);

                Matrix<Double> errorLayer_1 = multiplyEveryElement(weightDeltaLayer_2, secondLayerWeightsMatrix.translate());
                Matrix<Double> weightDeltaLayer_1 = scalarMultiplyMatrix(errorLayer_1,
                        evalFunctionForAllMatrixValues(firstLayerResultMatrix, ActivationFunctions::sigmoidDx));
                firstLayerWeightsMatrix = NeuralNetworkLearning.trainFullyConnectedLayer(firstLayerWeightsMatrix,
                        weightDeltaLayer_1, input.translate(), learningRate);
            }
        }

        Matrix<Double> firstLayerResultMatrix = multiplyMatrix(trainDataset.get(0), firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
        Matrix<Double> secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);

        System.out.println(secondLayerResultMatrix.getElement(0, 0));

        firstLayerResultMatrix = multiplyMatrix(trainDataset.get(1), firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
        secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);

        System.out.println(secondLayerResultMatrix.getElement(0, 0));

        firstLayerResultMatrix = multiplyMatrix(trainDataset.get(2), firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
        secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);

        System.out.println(secondLayerResultMatrix.getElement(0, 0));

        firstLayerResultMatrix = multiplyMatrix(trainDataset.get(3), firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
        secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);

        System.out.println(secondLayerResultMatrix.getElement(0, 0));

        firstLayerResultMatrix = multiplyMatrix(trainDataset.get(4), firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
        secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);

        System.out.println(secondLayerResultMatrix.getElement(0, 0));

        firstLayerResultMatrix = multiplyMatrix(trainDataset.get(5), firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
        secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);

        System.out.println(secondLayerResultMatrix.getElement(0, 0));

        firstLayerResultMatrix = multiplyMatrix(trainDataset.get(6), firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
        secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);

        System.out.println(secondLayerResultMatrix.getElement(0, 0));

        firstLayerResultMatrix = multiplyMatrix(trainDataset.get(7), firstLayerWeightsMatrix, ActivationFunctions::sigmoid);
        secondLayerResultMatrix = multiplyMatrix(firstLayerResultMatrix, secondLayerWeightsMatrix, ActivationFunctions::sigmoid);

        System.out.println(secondLayerResultMatrix.getElement(0, 0));


    }

    private static List<ParsedMatrix<Double>> createTrainDataset() {
        List<ParsedMatrix<Double>> trainDataset = new ArrayList<>();
        Double[][] input = {{0d, 0d, 0d}};
        Double[][] input1 = {{0d, 0d, 1d}};
        Double[][] input2 = {{0d, 1d, 0d}};
        Double[][] input3 = {{0d, 1d, 1d}};
        Double[][] input4 = {{1d, 0d, 0d}};
        Double[][] input5 = {{1d, 0d, 1d}};
        Double[][] input6 = {{1d, 1d, 0d}};
        Double[][] input7 = {{1d, 1d, 1d}};
        trainDataset.add(new ParsedMatrix<>(input, 0));
        trainDataset.add(new ParsedMatrix<>(input1, 1));
        trainDataset.add(new ParsedMatrix<>(input2, 0));
        trainDataset.add(new ParsedMatrix<>(input3, 0));
        trainDataset.add(new ParsedMatrix<>(input4, 1));
        trainDataset.add(new ParsedMatrix<>(input5, 1));
        trainDataset.add(new ParsedMatrix<>(input6, 0));
        trainDataset.add(new ParsedMatrix<>(input7, 0));
        return trainDataset;
    }

    private static Matrix<Double> createMatrixOneDouble(double value) {
        Matrix<Double> matrix = new Matrix<>();
        List<Double> list = new ArrayList<>();
        list.add(value);
        matrix.addRow(list);
        return matrix;
    }

    private static Matrix<Double> evalFunctionForAllMatrixValues(Matrix<Double> matrix, DoubleFunction<Double> function) {
        Matrix<Double> newMatrix = new Matrix<>();
        for (int y = 0; y < matrix.getSizeRow(); y++) {
            List<Double> row = new ArrayList<>();
            for (int x = 0; x < matrix.getSizeCol(); x++) {
                row.add(function.apply(matrix.getElement(x, y)));
            }
            newMatrix.addRow(row);
        }
        return newMatrix;
    }
}


