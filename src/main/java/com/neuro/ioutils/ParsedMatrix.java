package com.neuro.ioutils;

import com.neuro.math.Matrix;

import java.util.List;

public class ParsedMatrix<T extends Number> extends Matrix {
    private int label;

    public ParsedMatrix(List<T> matrix) {
        super(matrix);
    }

    public ParsedMatrix(T[][] matrix) {
        super(matrix);
    }

    public ParsedMatrix(T[][] matrix, int label) {
        super(matrix);
        this.label = label;
    }

    public ParsedMatrix() {
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }
}
