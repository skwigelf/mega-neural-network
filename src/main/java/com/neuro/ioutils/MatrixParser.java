package com.neuro.ioutils;

import com.neuro.math.Matrix;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MatrixParser  {

    public List<ParsedMatrix<Double>> readData(String dataFilePath, String labelFilePath) throws IOException {

        DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(getFileFromResources(dataFilePath))));
        int magicNumber = dataInputStream.readInt();
        int numberOfItems = dataInputStream.readInt();
        int nRows = dataInputStream.readInt();
        int nCols = dataInputStream.readInt();

        System.out.println("magic number is " + magicNumber);
        System.out.println("number of items is " + numberOfItems);
        System.out.println("number of rows is: " + nRows);
        System.out.println("number of cols is: " + nCols);

        DataInputStream labelInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(getFileFromResources(labelFilePath))));
        int labelMagicNumber = labelInputStream.readInt();
        int numberOfLabels = labelInputStream.readInt();

        System.out.println("labels magic number is: " + labelMagicNumber);
        System.out.println("number of labels is: " + numberOfLabels);

        List<ParsedMatrix<Double>> data = new ArrayList<>();

        for(int i = 0; i < numberOfItems; i++) {
            ParsedMatrix<Double> mnistMatrix = new ParsedMatrix<>();
            mnistMatrix.setLabel(labelInputStream.readUnsignedByte());
            for (int r = 0; r < nRows; r++) {
                List<Double> row = new ArrayList<>();
                for (int c = 0; c < nCols; c++) {
                    row.add((double) dataInputStream.readUnsignedByte());
                }
                mnistMatrix.addRow(row);
            }
            data.add(mnistMatrix);
        }
        dataInputStream.close();
        labelInputStream.close();
        return data;
    }

    private File getFileFromResources(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }

    }
}