package com.neuro;

import com.neuro.math.Matrix;
import com.neuro.neural.ActivationFunctions;
import com.neuro.neural.NeuralNetworkLearning;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleFunction;

import static com.neuro.math.MathUtilsDouble.scalarMultiplyMatrix;
import static org.junit.Assert.assertEquals;

public class NeuralNetworkLearningTest {

    @Test
    public void trainLayer() {
        Double[][] weights = {
                {0.5},
                {0.52}};
        Double[][] errors = {
                {0.69}};
        Double[][] values = {
                {0.77, 0.78}};
        Matrix<Double> weightsMatrix = new Matrix<>(weights);
        Matrix<Double> errorsMatrix = new Matrix<>(errors);
        Matrix<Double> valuesMatrix = new Matrix<>(values);
        double learningRate = 0.1;
        Matrix<Double> weightDeltaLayer_2 = scalarMultiplyMatrix(errorsMatrix,
                evalFunctionForAllMatrixValues(errorsMatrix, ActivationFunctions::sigmoidDx));
        Matrix<Double> result = NeuralNetworkLearning.trainFullyConnectedLayer(weightsMatrix, weightDeltaLayer_2, valuesMatrix,
                learningRate);

        assertEquals(2, result.getSizeCol());
    }

    private static Matrix<Double> evalFunctionForAllMatrixValues(Matrix<Double> matrix, DoubleFunction<Double> function) {
        Matrix<Double> newMatrix = new Matrix<>();
        for (int y = 0; y < matrix.getSizeRow(); y++) {
            List<Double> row = new ArrayList<>();
            for (int x = 0; x < matrix.getSizeCol(); x++) {
                row.add(function.apply(matrix.getElement(x, y)));
            }
            newMatrix.addRow(row);
        }
        return newMatrix;
    }
}
