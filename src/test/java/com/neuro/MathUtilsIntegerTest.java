package com.neuro;

import com.neuro.math.MathUtilsInteger;
import com.neuro.math.Matrix;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MathUtilsIntegerTest {
    private Matrix<Integer> firstMatrix;
    private Matrix<Integer> convolutionalMatrix;
    private Matrix<Integer> maxPoolMatrix;

    @Before
    public void setUp() {
        Integer[][] matrix = {
                {1, 1, 1, 1, 1},
                {0, 1, 0, 1, 1},
                {1, 1, 0, 1, 1},
                {0, 1, 1, 1, 0},
                {1, 1, 1, 1, 1}};
        firstMatrix = new Matrix<>(matrix);
        Integer[][] convolutional = {
                {1, 0, 1},
                {1, 0, 1},
                {1, 0, 1}};
        convolutionalMatrix = new Matrix<>(convolutional);
        Integer[][] maxPool = {
                {4, 2, 8, 5},
                {1, 3, 7, 8},
                {0, 5, 8, -5},
                {1, 2, 6, 5}};
        maxPoolMatrix = new Matrix<>(maxPool);
    }

    @Test
    public void multiplyConvolutionalLayer() {
        Matrix<Integer> resultMatrix = MathUtilsInteger.multiplyConvolutionalLayer(firstMatrix, convolutionalMatrix);

        assertMultiplyConvolutionalLayer(resultMatrix);
    }

    @Test
    public void maxPool() {
        Matrix<Integer> resultMatrix = MathUtilsInteger.maxPool(maxPoolMatrix, 2, 2);

        assertMaxPool(resultMatrix);
    }

    private void assertMultiplyConvolutionalLayer(Matrix<Integer> resultMatrix) {
        assertEquals(3, resultMatrix.getSizeCol());
        assertEquals(3, resultMatrix.getSizeRow());

        assertEquals(3, resultMatrix.getElement(0, 0).intValue());
        assertEquals(6, resultMatrix.getElement(1, 0).intValue());
        assertEquals(4, resultMatrix.getElement(2, 0).intValue());
        assertEquals(2, resultMatrix.getElement(0, 1).intValue());
        assertEquals(6, resultMatrix.getElement(1, 1).intValue());
        assertEquals(3, resultMatrix.getElement(2, 1).intValue());
        assertEquals(3, resultMatrix.getElement(0, 0).intValue());
        assertEquals(6, resultMatrix.getElement(1, 1).intValue());
        assertEquals(4, resultMatrix.getElement(2, 2).intValue());
    }

    private void assertMaxPool(Matrix<Integer> maxPoolMatrix) {
        assertEquals(2, maxPoolMatrix.getSizeCol());
        assertEquals(2, maxPoolMatrix.getSizeRow());

        assertEquals(4, maxPoolMatrix.getElement(0, 0).intValue());
        assertEquals(5, maxPoolMatrix.getElement(0, 1).intValue());
        assertEquals(8, maxPoolMatrix.getElement(1, 0).intValue());
        assertEquals(8, maxPoolMatrix.getElement(1, 1).intValue());
    }
}
