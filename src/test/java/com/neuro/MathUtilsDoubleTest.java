package com.neuro;

import com.neuro.math.MathUtilsDouble;
import com.neuro.math.Matrix;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MathUtilsDoubleTest {

    @Test
    public void convolutional() {
        Double[][] matrix = {
                {1d, 1d, 1d, 1d, 1d},
                {0d, 1d, 0d, 1d, 1d},
                {1d, 1d, 0d, 1d, 1d},
                {0d, 1d, 1d, 1d, 0d},
                {1d, 1d, 1d, 1d, 1d}};
        Matrix<Double> firstMatrix = new Matrix<>(matrix);
        Double[][] convolutional = {
                {1d, 0d, 1d},
                {1d, 0d, 1d},
                {1d, 0d, 1d}};
        Matrix<Double> convolutionalMatrix = new Matrix<>(convolutional);
        Double[][] maxPool = {
                {4d, 2d, 8d, 5d},
                {1d, 3d, 7d, 8d},
                {0d, 5d, 8d, -5d},
                {1d, 2d, 6d, 5d}};
        Matrix<Double> maxPoolMatrix = new Matrix<>(maxPool);

        Matrix<Double> resultMatrix = MathUtilsDouble.multiplyConvolutionalLayer(firstMatrix, convolutionalMatrix);

        assertMultiplyConvolutionalLayer(resultMatrix);
    }

    @Test
    public void multiplyMatrix() {
        Double[][] matrix = {
                {2d, 0d, 3d},
                {3d, 5d, 6d},
                {9d, 8d, 13d}};
        Matrix<Double> firstMatrix = new Matrix<>(matrix);
        Double[][] second = {
                {2d, 3d, 5d},
                {1d, 4d, 8d}};
        Matrix<Double> secondMatrix = new Matrix<>(second);

        Double[][] expectResult = {
                {58d, 55d, 89d},
                {86d, 84d, 131d}};
        Matrix<Double> expectMatrix = new Matrix<>(expectResult);

        Matrix<Double> resultMatrix = MathUtilsDouble.multiplyMatrix(secondMatrix, firstMatrix);

        for (int x = 0; x < expectMatrix.getSizeCol(); x++) {
            for (int y = 0; y < expectMatrix.getSizeRow(); y++) {
                assertEquals(expectMatrix.getElement(x, y), resultMatrix.getElement(x, y));
            }
        }
    }

    @Test
    public void maxPool() {
        //Matrix<Integer> resultMatrix = MathUtilsInteger.maxPool(maxPoolMatrix, 2, 2);

        //assertMaxPool(resultMatrix);
    }

    private void assertMultiplyConvolutionalLayer(Matrix<Double> resultMatrix) {
        assertEquals(3, resultMatrix.getSizeCol());
        assertEquals(3, resultMatrix.getSizeRow());

        assertEquals(3, resultMatrix.getElement(0, 0).doubleValue(), 0);
        assertEquals(6, resultMatrix.getElement(1, 0).doubleValue(), 0);
        assertEquals(4, resultMatrix.getElement(2, 0).doubleValue(), 0);
        assertEquals(2, resultMatrix.getElement(0, 1).doubleValue(), 0);
        assertEquals(6, resultMatrix.getElement(1, 1).doubleValue(), 0);
        assertEquals(3, resultMatrix.getElement(2, 1).doubleValue(), 0);
        assertEquals(3, resultMatrix.getElement(0, 0).doubleValue(), 0);
        assertEquals(6, resultMatrix.getElement(1, 1).doubleValue(), 0);
        assertEquals(4, resultMatrix.getElement(2, 2).doubleValue(), 0);
    }

    private void assertMaxPool(Matrix<Integer> maxPoolMatrix) {
        assertEquals(2, maxPoolMatrix.getSizeCol());
        assertEquals(2, maxPoolMatrix.getSizeRow());

        assertEquals(4, maxPoolMatrix.getElement(0, 0).doubleValue(), 0);
        assertEquals(5, maxPoolMatrix.getElement(0, 1).doubleValue(), 0);
        assertEquals(8, maxPoolMatrix.getElement(1, 0).doubleValue(), 0);
        assertEquals(8, maxPoolMatrix.getElement(1, 1).doubleValue(), 0);
    }
}
