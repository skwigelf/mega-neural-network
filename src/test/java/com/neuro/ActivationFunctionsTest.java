package com.neuro;

import com.neuro.neural.ActivationFunctions;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ActivationFunctionsTest {
    @Test
    public void functionReLu() {
        double positiveVal = 1.5;
        double negativeVal = -100;

        double positiveValResult = ActivationFunctions.reLu(positiveVal);
        double negativeValResult = ActivationFunctions.reLu(negativeVal);

        assertEquals(positiveVal, positiveValResult, 0);
        assertEquals(negativeVal, negativeValResult, -2 * negativeVal);
    }

    @Test
    public void functionSigmoid() {
        double firstResult = ActivationFunctions.sigmoid(0d);
        double secondResult = ActivationFunctions.sigmoid(2d);

        assertEquals(0.5, firstResult, 0);
        assertEquals(0.8807970757794222, secondResult, 0);
    }
}
